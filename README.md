# python docker pack #

## Features ##

* develop python with custom requirements using virtualenv from local dir (env)
* pack python app into docker image  

## Preparation ##

* Rename vagrant.yml.yml-dist to vagrant.yml 
* Modify/add synced folders items
* run `vagrant up --provision`
* run `vagrant ssh`

## Scenario ##

1. Build image **inside VM** ( -> How to build)
1. run required command **inside VM** ( -> Usages )

**inside VM** - you must login into VM via ssh

## How to build ##

```
./docker/build.sh [NAME]
```

## Usages ##

For development, how to install requirements into virtualenv folder

```
./run/install-reqs.sh /path/to/project/app/requirements.txt
```

For development, how to run single script

```
./run/script.sh /path/to/project/app/script.py [script args]
```

For development, how to run single script with requirements

```
./run/script-with-reqs.sh /path/to/project/app/requirements.txt /path/to/project/app/script.py [script args]
```

For convenience you should put you script and requirements into **app** folder inside /path/to/project

## How to pack for prod ##

As soon as your script is ready for prod and you've installed requirements into **env** folder via **run/install-reqs.sh** you can do

```
./run/pack.sh /path/to/project/app/ [image-name [tag]]
```

to create image **image-name:tag** with app and virtualenv sources inside it (in the /usr/share/py-docker-pack folder)

default values:
* image-name - py-docker-pack
* tag - latest

**/usr/share/py-docker-pack** is work dir for container

## How to run packed app ##

```
docker run --rm image-name /virtualenv-python.sh app/script.py
```

Or with mounted volume and script args

```
docker run --rm -v /path/to/volume/:/volume image-name /virtualenv-python.sh app/script.py [script args]
```
