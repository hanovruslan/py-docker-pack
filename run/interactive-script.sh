#!/usr/bin/env bash

self_path=$(readlink -f "$0")
self_dir=$(dirname ${self_path})

source ${self_dir}/source.sh
source ${self_dir}/../env/main.env

from_work_dir=$(dirname ${self_dir})
path_to_source=${1}
from_source_dir=$(dirname ${path_to_source})
to_source_dir=$(basename ${from_source_dir})
source=$(basename ${path_to_source})
options=${@:2}

py_docker_pack_run_interactive_script \
  ${from_work_dir} \
  ${from_source_dir} \
  ${to_source_dir} \
  ${source} \
  "${options}" \
&& sudo rm -df ${from_work_dir}/${to_source_dir}
