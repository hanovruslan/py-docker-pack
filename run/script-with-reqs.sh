#!/usr/bin/env bash

self_path=$(readlink -f "$0")
self_dir=$(dirname ${self_path})

source ${self_dir}/source.sh
source ${self_dir}/../env/main.env

from_work_dir=$(dirname ${self_dir})
path_to_source_requirements=${1}
path_to_source=${2}
from_source_dir=$(dirname ${path_to_source})
to_source_dir=$(basename ${from_source_dir})
requirements=$(basename ${path_to_source_requirements})
source=$(basename ${path_to_source})
options=${@:3}

py_docker_pack_run_script_with_requirements \
  ${from_work_dir} \
  ${from_source_dir} \
  ${to_source_dir} \
  ${requirements} \
  ${source} \
  "${options}" \
&& sudo rm -df ${to_source_dir}