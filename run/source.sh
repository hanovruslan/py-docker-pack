#!/usr/bin/env bash

function py_docker_pack_run_script {
  local from_work_dir=${1}
  local from_source_dir=${2}
  local to_source_dir=${3}
  local source=${4}
  local options=${5}
  local cmd_run;

  cmd_run=$(py_docker_pack_cmd_run \
    ${from_work_dir} \
    ${from_source_dir} \
    ${to_source_dir})

  ${cmd_run} ${PY_DOCKER_PACK_PYTHON_CMD} ${to_source_dir}/${source} ${options}
}

function py_docker_pack_run_interactive_script {
  local from_work_dir=${1}
  local from_source_dir=${2}
  local to_source_dir=${3}
  local source=${4}
  local options=${5}
  local cmd_run;

  cmd_run=$(py_docker_pack_cmd_run \
    ${from_work_dir} \
    ${from_source_dir} \
    ${to_source_dir} \
    1)

  ${cmd_run} ${PY_DOCKER_PACK_PYTHON_CMD} ${to_source_dir}/${source} ${options}
}
function py_docker_pack_install_requirements {
  local from_work_dir=${1}
  local from_source_dir=${2}
  local to_source_dir=${3}
  local requirements=${4}
  local cmd_run;

  cmd_run=$(py_docker_pack_cmd_run \
    ${from_work_dir} \
    ${from_source_dir} \
    ${to_source_dir})

  ${cmd_run} ${PY_DOCKER_PACK_PIP_CMD} install -r ${to_source_dir}/${requirements}
}
function py_docker_pack_run_script_with_requirements {
  local from_work_dir=${1}
  local from_source_dir=${2}
  local to_source_dir=${3}
  local requirements=${4}
  local source=${5}
  local options=${6}
  local cmd_run;

  cmd_run=$(py_docker_pack_cmd_run \
    ${from_work_dir} \
    ${from_source_dir} \
    ${to_source_dir})

  ${cmd_run} ${PY_DOCKER_PACK_PIP_CMD} install -r ${to_source_dir}/${requirements} \
  && ${cmd_run} ${PY_DOCKER_PACK_PYTHON_CMD} ${to_source_dir}/${source} ${options}

}
function py_docker_pack_pack {
  local from_work_dir=${1}
  local from_source_dir=${2}
  local to_source_dir=${3}
  local app_name=${4}
  local tag=${5}
  local container=${app_name}
  local cmd_daemonize;
  local cmd_commit_container;

  cmd_daemonize=$(py_docker_pack_cmd_daemonize ${container})
  cmd_cp_env=$(py_docker_pack_cmd_cp ${from_work_dir}/${PY_DOCKER_PACK_ENV_DIR} ${container} ${PY_DOCKER_PACK_RUN_DIR}/)
  cmd_cp_source=$(py_docker_pack_cmd_cp ${from_source_dir} ${container} ${PY_DOCKER_PACK_RUN_DIR}/)
  cmd_commit_container=$(py_docker_pack_cmd_commit_container ${container} ${tag})
  cmd_get_image_id=$(py_docker_pack_cmd_get_image_id ${container} ${tag})

  ${cmd_daemonize} \
  && ${cmd_cp_env} \
  && ${cmd_cp_source} \
  && ${cmd_commit_container} \
  && py_docker_pack_cmd_tag_image ${container} ${tag} $(${cmd_get_image_id}) \
  && docker stop ${container} \
  && docker rm ${container}
}
function py_docker_pack_cmd_daemonize {
  local container=${1}
  local cmd;

  cmd="docker run -d --name ${1} ${PY_DOCKER_PACK_IMAGE} tail -f /dev/null"

  echo ${cmd}
}
function py_docker_pack_cmd_commit_container {
  local container=${1}
  local tag=${2}
  local cmd;

  cmd="docker commit -p ${container} ${container}:${tag}"

  echo ${cmd}
}
function py_docker_pack_cmd_cp {
  local from=${1}
  local container=${2}
  local to=${3}

  cmd="docker cp ${from} ${container}:${to}"

  echo ${cmd}
}
function py_docker_pack_cmd_tag_image {
  local image=${1}
  local tag=${2}
  local id=${3}
  local cmd;

  cmd="docker tag ${id} ${image}:${tag}"

  echo ${cmd}
}
function py_docker_pack_cmd_get_image_id {
  local image=${1}
  local tag=${2}
  local cmd;

  cmd="docker images --format {{.ID}} ${image}:${tag}"

  echo ${cmd}
}
function py_docker_pack_cmd_run {
  local from_work_dir=${1}
  local from_source_dir=${2}
  local to_source_dir=${3}
  local is_interactive=${4:-0}
  local cmd="docker run --rm ";

  if [ ${is_interactive} -eq 1 ]
    then cmd="${cmd} -ti "
  fi
  cmd="${cmd} \
    -v ${from_work_dir}:${PY_DOCKER_PACK_RUN_DIR} \
    -v ${from_source_dir}:${PY_DOCKER_PACK_RUN_DIR}/${to_source_dir} \
    ${PY_DOCKER_PACK_IMAGE}"

  echo ${cmd}
}
