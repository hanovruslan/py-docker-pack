#!/usr/bin/env bash

self_path=$(readlink -f "$0")
self_dir=$(dirname ${self_path})

source ${self_dir}/source.sh
source ${self_dir}/../env/main.env

from_source_dir=${1%%/}
app_name=${2:-${PY_DOCKER_PACK_IMAGE}}
tag=${3:-latest}
from_work_dir=$(dirname ${self_dir})
to_source_dir=$(basename ${from_source_dir})

py_docker_pack_pack \
  ${from_work_dir} \
  ${from_source_dir} \
  ${to_source_dir} \
  ${app_name} \
  ${tag}