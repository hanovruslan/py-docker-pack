#!/usr/bin/env bash

function touch_virtualenv {
  local dest_dir=${1}

  [ $(check_virtualenv ${dest_dir}) -eq 1 ] || virtualenv ${dest_dir}
}
function check_virtualenv {
  local dest_dir=${1}

  echo $( [ -f ${dest_dir}/${BIN_PIP} ] && echo 1 || echo 0 )
}
function run_bin {
  local bin=${1}
  local argv=${@:2}

  touch_virtualenv ${WORKDIR}/${ENV} \
  && ${WORKDIR}/${ENV}/${bin} ${argv}
}
