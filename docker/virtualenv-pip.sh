#!/usr/bin/env bash

self_path=$(readlink -f "$0")
self_dir=$(dirname ${self_path})

source ${self_dir}/virtualenv-source.sh

run_bin ${BIN_PIP} ${@}
