#!/usr/bin/env bash

self_path=$(readlink -f "$0")
self_dir=$(dirname ${self_path})

source ${self_dir}/../env/main.env

name=${1:-${PY_DOCKER_PACK_IMAGE}}
path=$(dirname $(readlink -f "$0"))

docker build -t ${name} ${path} \
    --build-arg WORKDIR=${PY_DOCKER_PACK_RUN_DIR} \
    --build-arg ENV=${PY_DOCKER_PACK_ENV_DIR}

